module.exports = function defaultFunction( grunt ){
    "use strict";

    /** This is what will run if you don't give Grunt any directions **/
    grunt.registerTask( "default", function defaultTask(){
        var tasks = [
            "sass",
            "webpack:cards"
        ];

        grunt.task.run( tasks );
    } );
};
