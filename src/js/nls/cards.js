// Internal Components
import I18n from "../helpers/I18n";
import Cards from "../../data/translations/cards.json";

var i18n = new I18n();

i18n.loadDefinitions(
    {
        "cards": Cards
    }
);

export default i18n.localize();
