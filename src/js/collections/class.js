// Extenal Libraries
import Backbone from "../bootstrappers/backbone";

// Internal Components
import ClassData from "../../data/class.json";

var ClassCollection = Backbone.Collection.extend( {
    fetch(){
        return this.set( ClassData );
    }
} );

export default ClassCollection;
