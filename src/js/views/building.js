// External Libraries
import Backbone from "../bootstrappers/backbone";
import _ from "lodash";
import $ from "jquery";

// Internal Components
import BuildingCollection from "../collections/building";
import TeacherCard from "./cards/teacher";
import ClassesView from "./classes";

// Template and Translation
import template from "../../templates/grid.html";
import translations from "../nls/cards";

var BuildingView = Backbone.Epoxy.View.extend( {
    "template": _.template( template ),
    "events": {},
    "bindings": {
        ".cards": "collection:$collection,itemView:'teacherCard'"
    },
    "teacherCard": TeacherCard,

    initialize( attributes ){
        this.collection = new BuildingCollection();
        this.collection.fetch();

        this.buildingName = attributes.model.get( "name" );

        this.listenTo(
            this.collection,
            "click:card",
            ( card ) => {
                var classesView = new ClassesView( card );

                $( ".container" ).html( classesView.$el );
            }
        );

        this.render();
    },

    render(){
        this.$el.html( this.template( {
            "title": this.buildingName
        } ) );

        return this;
    }
} );

export default BuildingView;
