// External Libraries
import _ from "lodash";

// Internal Components
import CardComponentView from "../Mini";
import template from "../../../templates/class/mini.html";

var MiniClassCardComponentView = CardComponentView.extend( {
    "template": _.template( template ),

    "bindings": {
        "h3.name": "text:className"
    },
    "events": {
        "click .card": function cardClickHandler(){
            this.model.trigger( "click:card", {
                "model": this.model
            } );
        }
    },

    "computeds": {
        "useDefaultPicture": {
            "deps": [ "pictures" ],
            "get": function useDefaultPictureGetter( picture ){
                return !picture;
            }
        }
    },

    initialize( constructionData ){
        this.model = constructionData.model;

        this.modifiers = constructionData.collectionView.cardModifiers || constructionData.modifiers || [];

        this.render();
    }
} );

export default MiniClassCardComponentView;
