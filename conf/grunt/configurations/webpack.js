var defaultConf = {
    "entry": "./src/js/cards.js",
    "output": {
        "path": "./build/js/",
        "filename": "cards.js"
    },
    "module": {
        "loaders": [
            {
                "loader": "babel",
                "test": /\.[ej]s[6]?$/,
                "exclude": [
                    "node_modules"
                ],
                "query": {
                    "presets": [ "es2015" ]
                }
            },
            {
                "loader": "html",
                "test": /\.html$/
            },
            {
                "loader": "json",
                "test": /\.json$/
            }
        ]
    },
    "htmlLoader": {
        "attrs": false
    },
    "devtool": "source-maps",
    "failOnError": true,
    "stats": {
        "colors": true,
        "modules": true,
        "reasons": true
    }
};

module.exports = function webpackConfigLoader(){
    return {
        "cards": defaultConf
    };
};
