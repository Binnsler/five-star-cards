// External Libraries
import Backbone from "../bootstrappers/backbone";

// Internal Components
import BuildingData from "../../data/building.json";

var BuildingCollection = Backbone.Collection.extend( {
    fetch(){
        return this.set( BuildingData );
    }
} );

export default BuildingCollection;
