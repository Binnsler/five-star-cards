## Cards Page

This activity will get you working with a Pivot-esque group of cards. You won't be hitting an API for this... instead, you'll need to pull data from the included data files (one per collection). You'll also have groups of cards, but the data will only populate the following path:

`(buildings page) -> Morgan Elementary -> Angela Martin -> Citizenship`

Try to emulate the styles found in the Pivot-1.5 repo, using the latest ES6 features.
