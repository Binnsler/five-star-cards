// External Libraries
import Backbone from "../bootstrappers/backbone";
import _ from "lodash";
import $ from "jquery";

// Internal Components
import ClassCollection from "../collections/class.js";
import StudentCard from "./cards/student";

// Template and Translations
import template from "../../templates/grid.html";
import translations from "../nls/cards";

var ClassView = Backbone.Epoxy.View.extend( {
    "template": _.template( template ),
    "events": {},
    "bindings": {
        ".cards": "collection:$collection,itemView:'studentCard'"
    },
    "studentCard": StudentCard,

    initialize( attributes ){
        this.collection = new ClassCollection();
        this.collection.fetch();

        this.className = attributes.model.get( "className" );

        this.listenTo(
            this.collection,
            "click:card",
            () => $( ".container" ).html( "<h1>Individual Student View</h1>" )
        );

        this.render();
    },

    render(){
        this.$el.html( this.template( {
            "title": this.className
        } ) );
    }
} );

export default ClassView;
