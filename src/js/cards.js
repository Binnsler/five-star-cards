// External Libraries
import $ from "jquery";

// Internal Components
import BuildingsView from "./views/buildings";

var buildings = new BuildingsView();

$( ".container" ).html( buildings.$el );
