// External Libraries
import Backbone from "../bootstrappers/backbone";

// Internal Components
import BuildingsData from "../../data/buildings.json";

var BuildingsCollection = Backbone.Collection.extend( {
    fetch(){
        return this.set( BuildingsData );
    }
} );


export default BuildingsCollection;
