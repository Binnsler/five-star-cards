// External Libraries
import Backbone from "../bootstrappers/backbone";

// Internal Components
import ClassesData from "../../data/classes.json";

var ClassesCollection = Backbone.Collection.extend( {
    fetch(){
        return this.set( ClassesData );
    }
} );

export default ClassesCollection;
