// External Libraries
import Backbone from "../bootstrappers/backbone";
import _ from "lodash";
import $ from "jquery";

// Internal Components
import BuildingsCollection from "../collections/buildings";
import BuildingCard from "./cards/building";
import BuildingView from "./building";

// Template and Translations
import template from "../../templates/grid.html";
import translations from "../nls/cards";

var BuildingsView = Backbone.Epoxy.View.extend( {
    "template": _.template( template ),
    "events": {},
    "bindings": {
        ".cards": "collection:$collection,itemView:'buildingCard'"
    },
    "buildingCard": BuildingCard,

    initialize(){
        this.collection = new BuildingsCollection();
        this.collection.fetch();

        this.listenTo(
            this.collection,
            "click:card",
            ( card ) => {
                var building = new BuildingView( card );

                $( ".container" ).html( building.$el );
            }
        );

        this.render();
    },
    render(){
        this.$el.html( this.template( {
            "title": translations.cards.titles.buildings
        } ) );

        return this;
    }
} );

export default BuildingsView;
