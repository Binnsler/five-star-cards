// Libraries
import _ from "lodash";

// Internal Components
import CardComponentView from "../Mini";
import template from "../../../templates/teacher/mini.html";

var MiniTeacherCardComponentView = CardComponentView.extend( {
    "template": _.template( template ),

    "bindings": {
        "h3.name": "text:fullNameParams",
        ".visual-identity img": "toggle:pictureUrl"
    },
    "events": {
        "click .card": function cardClickHandler(){
            this.model.trigger( "click:card", {
                "model": this.model
            } );
        }
    },

    "computeds": {
        "fullNameParams": {
            "deps": [ "firstName", "lastName" ],
            "get": function getFullName( firstName, lastName ){
                return firstName + " " + lastName;
            }
        }
    },

    initialize( constructionData ){
        this.model = constructionData.model;

        this.modifiers = constructionData.collectionView.cardModifiers || constructionData.modifiers || [];

        this.render();
    }
} );

export default MiniTeacherCardComponentView;
