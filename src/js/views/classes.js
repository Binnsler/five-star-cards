// External Libraries
import Backbone from "../bootstrappers/backbone";
import _ from "lodash";
import $ from "jquery";

// Internal Components
import ClassesCollection from "../collections/classes";
import ClassCard from "./cards/class";
import ClassView from "./class";

// Template and Translations
import template from "../../templates/grid.html";
import translations from "../nls/cards";

var ClassesView = Backbone.Epoxy.View.extend( {
    "template": _.template( template ),
    "events": {},
    "bindings": {
        ".cards": "collection:$collection,itemView:'classCard'"
    },
    "classCard": ClassCard,

    initialize( attributes ){
        this.collection = new ClassesCollection();
        this.collection.fetch();

        this.teacherName = attributes.model.get( "firstName" ) + " " + attributes.model.get( "lastName" );

        this.listenTo(
            this.collection,
            "click:card",
            ( card ) => {
                var classView = new ClassView( card );

                $( ".container" ).html( classView.$el );
            }

        );

        this.render();
    },

    render(){
        this.$el.html( this.template( {
            "title": this.teacherName
        } ) );

        return this;
    }
} );

export default ClassesView;
