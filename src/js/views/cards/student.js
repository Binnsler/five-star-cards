// External Libraries
import _ from "lodash";

// Internal Components
import CardComponentView from "../Mini";
import template from "../../../templates/student/mini.html";

var MiniStudentCardComponentView = CardComponentView.extend( {
    "template": _.template( template ),

    "bindings": _.extend( {}, CardComponentView.prototype.bindings, {
        "h3.name": "text:fullNameParams",
        ".visual-identity img": "toggle:picture"
    } ),
    "events": _.extend( {}, CardComponentView.prototype.events, {
        "click .card": function cardClickHandler(){
            this.model.trigger( "click:card", {
                "model": this.model
            } );
        }
    } ),

    "computeds": {
        "fullNameParams": {
            "deps": [ "firstName", "lastName" ],
            "get": function getFullName( firstName, lastName ){
                return firstName + " " + lastName;
            }
        }
    }
} );

export default MiniStudentCardComponentView;
